package Model;

import Controller.API;
import Controller.MainFrameController;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String... args){
        //input stream
        Scanner sc = new Scanner(System.in);
        //api url
        //map
        String apimapstring = "https://www.datos.gov.co/resource/p95u-vi7k.json";
        //bibliored
        String apibibliostring = "https://www.datos.gov.co/resource/in3j-awgi.json";
        //api objects
        API apimap = new API(apimapstring);
        API apibiblio = new API(apibibliostring);
        //connect with the recources
        apimap.connect();
        apibiblio.connect();
        HashMap deparments = apimap.queryFromAll("c_digo_dane_del_departamento","departamento");
//        System.out.println(deparments.toString());

        //user interface
        MainFrameController mfc = new MainFrameController(apimap,apibiblio);
        mfc.full1(deparments);
        mfc.ShowWindow();
    }
}
