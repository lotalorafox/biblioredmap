package View;

import javax.swing.*;

public class UserWindow extends JFrame {
    public static final int WIDTH =1200;
    public static final int HEIGHT =500;
    private JPanel MainPanel;
    private JList list1;
    private JList list2;
    private JList list3;
    private JButton sentButton;
    private JButton sentButton1;
    private JLabel label1;

    public UserWindow(){
        setSize(WIDTH,HEIGHT);
        setContentPane(MainPanel);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Bibliored catalogo");
    }

    public JLabel getLabel1() {
        return label1;
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }

    public JList getList1() {
        return list1;
    }

    public JList getList2() {
        return list2;
    }

    public JList getList3() {
        return list3;
    }

    public JButton getSentButton() {
        return sentButton;
    }

    public JButton getSentButton1() {
        return sentButton1;
    }
}
