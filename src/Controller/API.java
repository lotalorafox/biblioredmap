package Controller;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

public class API {
    String urltext;
    URL url;
    String[] fields;
    public API(String direction){
        this.urltext = direction;
    }
    public void connect(){
        this.url = this.getUrl(this.urltext);
    }
    public String getJsonString(URL u){
        Scanner in  = null;
        String j = "";
        try {
            in = new Scanner(u.openStream());
            while (in.hasNext()) {
                j += in.nextLine();
            }
        } catch (IOException e) {
            System.out.println("Not internet connection");
        }
        return j;
    }
    public URL getUrl(String r){
        URL w = null;
        try {
            w= new URL(r);
        } catch (MalformedURLException e) {
            System.out.println("The url is not valid");
        }
        return w;
    }
    public HashMap query(String field, String fvalue,String key,String value){
        HashMap q;
        String c = this.urltext + "?" + field + "=" + fvalue;
        URL u = this.getUrl(c);
        String jsonquery = this.getJsonString(u);
        q = this.getFromArray(this.par(jsonquery),key,value);
        return q;
    }
    public HashMap queryFromAll(String key,String value){
        HashMap q;
        String jsonquery = this.getJsonString(this.url);
        q = this.getFromArray(this.par(jsonquery),key,value);
        return q;
    }
    public JSONArray par(String j){
        JSONParser p = new JSONParser();
        JSONArray obj =null;
        try {
            obj = (JSONArray) p.parse(j);
        } catch (ParseException e) {
            System.out.println("Not connection or not json url");
        }
        return obj;
    }
    public HashMap getFromArray(JSONArray a,String key,String value){
        HashMap<String,String> r = new HashMap<>();
        try {
            for (Object i : a) {
                JSONObject h = (JSONObject) i;
                if (!(r.containsKey(h.get(key)))) {
                    r.put((String) h.get(key), (String) h.get(value));
                }
            }
        }catch (java.lang.NullPointerException e){
            System.out.println("not json object");
        }
        return r;
    }
    public HashMap getAllData(String find){
        HashMap<String,String> r = new HashMap<>();
        JSONArray a = this.par(this.getJsonString(this.url));
        int j =0;
        try {
            for (Object i : a) {
                JSONObject h = (JSONObject) i;
                String tem = h.toString();
                if (tem.contains(find)) {
                    r.put(Integer.toString(j), tem);
                    j++;
                }
            }
        }catch (java.lang.NullPointerException e){
            System.out.println("not json object");
        }
        return r;
    }
}
