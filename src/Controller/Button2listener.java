package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class Button2listener implements ActionListener {
    MainFrameController l;
    public Button2listener(MainFrameController m) {
        l=m;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] f = l.getList2().getSelectedValue().split(" ");
        String a = f[0]+"000";
        HashMap tem = l.getA2().getAllData(a);
        l.full3(tem);
    }
}
