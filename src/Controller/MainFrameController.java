package Controller;

import View.UserWindow;
import sun.font.TextLabel;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

public class MainFrameController {
    public API A1,A2;
    public UserWindow ui;
    private JList<String> list1;
    private JList<String> list2;
    private JList<String> list3;
    private JButton sentButton;
    private JButton sentButton1;
    private JLabel icon;
    public  MainFrameController(API a1,API a2){
        this.A1 = a1;
        this.A2 = a2;
        ui = new UserWindow();
        initcom();
        initlisteners();
    }

    private void initlisteners() {
        sentButton.addActionListener(new Button1listener(this));
        sentButton1.addActionListener(new Button2listener(this));
    }

    public void ShowWindow(){
        this.ui.setVisible(true);
    }
    private void initcom() {
        this.list1 = ui.getList1();
        list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.list2 = ui.getList2();
        list2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.list3 = ui.getList3();
        list3.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.sentButton = ui.getSentButton();
        this.sentButton1 = ui.getSentButton1();
        this.icon = ui.getLabel1();
        ImageIcon image = new ImageIcon("img/logo.png");
        icon.setIcon(image);
    }
    public void full1(HashMap<String,String> a){
        DefaultListModel<String> listModel = new DefaultListModel<>();
        Set<String> k = a.keySet();
        for(String w :k){
            String tem = w + " " + a.get(w);
            listModel.addElement(tem);
        }
        list1.setModel(listModel);
    }
    public void full2(HashMap<String,String> a){
        DefaultListModel<String> listModel2 = new DefaultListModel<>();
        Set<String> k = a.keySet();
        for(String w :k){
            String tem = w + " " + a.get(w);
            listModel2.addElement(tem);
        }
        list2.setModel(listModel2);
    }
    public void full3(HashMap<String,String> a){
        DefaultListModel<String> listModel = new DefaultListModel<>();
        Set<String> k = a.keySet();
        System.out.println("Finding libraries");
        for(String w :k){
            String l = a.get(w);
            l=l.replace("{"," ");
            l=l.replace("}"," ");
            l=l.replace(',',' ');
            System.out.println(l);
            String tem = w + " " + a.get(w) + "\n";
            listModel.addElement(tem);
        }
        list3.setModel(listModel);
    }

    public UserWindow getUi() {
        return ui;
    }

    public JList<String> getList1() {
        return list1;
    }

    public JList<String> getList2() {
        return list2;
    }

    public JList<String> getList3() {
        return list3;
    }

    public JButton getSentButton() {
        return sentButton;
    }

    public JButton getSentButton1() {
        return sentButton1;
    }

    public API getA1() {
        return A1;
    }

    public API getA2() {
        return A2;
    }
}
